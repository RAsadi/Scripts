set -e

PACKAGE_MANAGER=$1

if [ "$PACKAGE_MANAGER" != "brew" ]; then
    PACKAGE_MANAGER = "sudo $PACKAGE_MANAGER"
fi

#Getting curl for future use
$PACKAGE_MANAGER install curl

#Gitting git, really i should probably have git if i can even get this
$PACKAGE_MANAGER install git

#Make shell into oh-my-zsh
$PACKAGE_MANAGER install zsh
chsh -s $(which zsh)
sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"

#Now that we are using zsh, add this folder to the path
cat "$PWD" >> ~/.zshrc
source ~/.zshrc

#Getting vim
$PACKAGE_MANAGER install vim
#this is just the popular vimrc, not cool enough to make my own yet
git clone --depth=1 https://github.com/amix/vimrc.git ~/.vim_runtime
sh ~/.vim_runtime/install_awesome_vimrc.sh

#Getting diff-so-fancy
#this should already be in the path as i put the diff-so-fancy script in the same folder
git config --global core.pager "diff-so-fancy | less --tabs=4 -RFX"
git config --global color.ui true

git config --global color.diff-highlight.oldNormal    "red bold"
git config --global color.diff-highlight.oldHighlight "red bold 52"
git config --global color.diff-highlight.newNormal    "green bold"
git config --global color.diff-highlight.newHighlight "green bold 22"

git config --global color.diff.meta       "yellow"
git config --global color.diff.frag       "magenta bold"
git config --global color.diff.commit     "yellow bold"
git config --global color.diff.old        "red bold"
git config --global color.diff.new        "green bold"
git config --global color.diff.whitespace "red reverse"

#getting monokai terminal
git clone git://github.com/stephenway/monokai.terminal.git ~/monokai

#Getting various random languages and other stuff
$PACKAGE_MANAGER install java
$PACKAGE_MANAGER install python
$PACKAGE_MANAGER install python3
$PACKAGE_MANAGER install scala
#things got too heavy
#$PACKAGE_MANAGER install maven
#$PACKAGE_MANAGER install gradle
#$PACKAGE_MANAGER install sbt
#Also get vscode probably
